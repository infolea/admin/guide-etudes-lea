# LeaGuide

Application Node.js / Web  pour générer le guide des études à partir des fiches UE exportées depuis une base de données Moodle. 

## Nécessite 
* Node.js 
* npm

## Utilisation
* Téléchargez la liste des UEs et placez la dans input/fiches.csv
* Remplissez les fichiers d'introduction input/intro.md ...
* Personnalisez le fichier de style input/reference.odt
* Entrez les commandes suivantes:
	* npm install
	* npm start
* Les fichiers générés sont placés dans le dossier output

