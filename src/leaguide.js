"use strict";

/*

Pour extraire les guides, on aurait donc les cas suivants:
guide L1: recherche année="L1", ensuite ordre des numéros d'UE, et à l'intérieur l'intitulé, alphabétique, ce qui suffit pour regrouper les enseignements par langue
guide L2: idem, mais recherche "L2"
guide L3 LEA: idem, mais recherche année="L3"+parcours="appliqués" (pour avoir toutes les combinaisons des langues)
guide L3 MPT: recherche année="L3"+parcours="touristiques"; si même fiche a aussi parcours "appliqués", renuméroter UE+autre nom UE en cas de besoin, puis ordre des numéros d'UE et à l'intérieur, ordre alphabétique
guide L3 TCI: même chose que MPT, mais recherche "L3" + "techniques"

Ca peut marcher et surtout: ca me donne un peu moins de travail pour préparer les fiches. Au niveau des masters, les doubles parcours seraient aussi une solution pour le tronc commun et pour les enseignements partagés entre les contrats pro/apprentis et les autres.
*/
/*
M1 ANI: année=M1 AND parcours= "Master LEA - Tronc commun" OR parcours="Master LEA - Projet de l'étudiant" OR parcours="Affaires et Négociation Internationales" Ordre: numéro UE, puis alphabétique pour les EC

M1 ANI pro: année=M1 AND parcours= "Master LEA - Tronc commun" OR parcours="Master Apprentissage - Contrat Pro - Tronc commun" OR parcours="Affaires et négociation internationales - En Contrat pro". IF mêmes EC avec parcours = "Master LEA - Tronc commun" AND parcours="Master Apprentissage - Contrat Pro - Tronc commun", THEN supprimer les fiches parcours = "Master LEA - Tronc commun" concernées.  Ordre: numéro UE, puis alphabétique pour les EC

M1 TCI: année=M1 AND parcours= "Master LEA - Tronc commun" OR parcours="Master LEA - Projet de l'étudiant" OR parcours="Techniques du commerce international" Ordre: numéro UE, puis alphabétique pour les EC

M1 TCI pro: année=M1 AND parcours= "Master LEA - Tronc commun" OR parcours="Master Apprentissage - Contrat Pro - Tronc commun" OR parcours="Techniques du Commerce International - En Contrat pro" IF mêmes EC avec parcours = "Master LEA - Tronc commun" AND parcours="Master Apprentissage - Contrat Pro - Tronc commun", THEN supprimer les fiches parcours = "Master LEA - Tronc commun" concernées.  Ordre: numéro UE, puis alphabétique pour les EC

M1 MPT: année=M1 AND parcours= "Master LEA - Tronc commun" OR parcours="Master LEA - Projet de l'étudiant" OR parcours="Affaires et Négociation Internationales" Ordre: numéro UE, puis alphabétique pour les EC

M1 MPTA: année=M1 AND parcours= "Master LEA - Tronc commun" OR parcours="Master Apprentissage - Contrat Pro - Tronc commun" OR parcours="Management de projets touristiques - En Apprentissage" IF mêmes EC avec parcours = "Master LEA - Tronc commun" AND parcours="Master Apprentissage - Contrat Pro - Tronc commun", THEN supprimer les fiches du parcours = "Master LEA - Tronc commun" concernées.  Ordre: numéro UE, puis alphabétique pour les EC

M1 RICI: année=M1 AND parcours= "Master LEA - Tronc commun" OR parcours="Master LEA - Projet de l'étudiant" OR parcours="Relations interculturelles et coopération internationale" Ordre: numéro UE, puis alphabétique pour les EC

M2 ANI: année=M2 AND parcours= "Master LEA - Tronc commun" OR parcours="Master LEA - Projet de l'étudiant" OR parcours="Affaires et Négociation Internationales" Ordre: numéro UE, puis alphabétique pour les EC

M2 ANI pro: année=M2 AND parcours= "Master LEA - Tronc commun" OR parcours="Master Apprentissage - Contrat Pro - Tronc commun" OR parcours="Affaires et négociation internationales - En Contrat pro". IF mêmes EC avec parcours = "Master LEA - Tronc commun" AND parcours="Master Apprentissage - Contrat Pro - Tronc commun", THEN supprimer les fiches parcours = "Master LEA - Tronc commun" concernées.  Ordre: numéro UE, puis alphabétique pour les EC

M2 TCI: année=M2 AND parcours= "Master LEA - Tronc commun" OR parcours="Master LEA - Projet de l'étudiant" OR parcours="Techniques du commerce international" Ordre: numéro UE, puis alphabétique pour les EC

M2 TCI pro: année=M2 AND parcours= "Master LEA - Tronc commun" OR parcours="Master Apprentissage - Contrat Pro - Tronc commun" OR parcours="Techniques du Commerce International - En Contrat pro" IF mêmes EC avec parcours = "Master LEA - Tronc commun" AND parcours="Master Apprentissage - Contrat Pro - Tronc commun", THEN supprimer les fiches parcours = "Master LEA - Tronc commun" concernées.  Ordre: numéro UE, puis alphabétique pour les EC

M2 MPT: année=M2 AND parcours= "Master LEA - Tronc commun" OR parcours="Master LEA - Projet de l'étudiant" OR parcours="Affaires et Négociation Internationales" Ordre: numéro UE, puis alphabétique pour les EC

M2 MPTA: année=M2 AND parcours= "Master LEA - Tronc commun" OR parcours="Master Apprentissage - Contrat Pro - Tronc commun" OR parcours="Management de projets touristiques - En Apprentissage" IF mêmes EC avec parcours = "Master LEA - Tronc commun" AND parcours="Master Apprentissage - Contrat Pro - Tronc commun", THEN supprimer les fiches du parcours = "Master LEA - Tronc commun" concernées.  Ordre: numéro UE, puis alphabétique pour les EC

M2 RICI: année=M2 AND parcours= "Master LEA - Tronc commun" OR parcours="Master LEA - Projet de l'étudiant" OR parcours="Relations interculturelles et coopération internationale" Ordre: numéro UE, puis alphabétique pour les EC
*/





//Filtrer les fiches 


let Papa = require("papaparse");

let fs = require("fs");

//const jsdom = require("jsdom");

const cheerio = require('cheerio');

var JSZip = require("jszip");
var zip = new JSZip();

const { exec } = require("child_process");

const csvFile = "input/fiches.csv";

//Filters/renaming per year/specialisation
let guides = new Map();
let guidesFilters = new Map();
guidesFilters.set("Master TSM M1", new Map([
    ["Enseignement", ["Nom EC"] ],
    ["Responsable", ["Enseignant.e.s"]],
    ["Pré-requis", ["Prérequis"]],
    ["Descriptif du cours", ["Descriptif"]],
    ["Compétence(s) visée(s)", ["Compétence(s) visée(s)"]],
    ["Contrôle des connaissances", ["Informations sur l'évaluation"]],
    ["Langue d'enseignement", ["Langue d'enseignement"]],
    ["Bibliographie sélective", ["Bibliographie sélective"]]
]));
guidesFilters.set("Master TSM M2", new Map([
    ["Enseignement", ["Nom EC"] ],
    ["Responsable", ["Enseignant.e.s"]],
    ["Pré-requis", ["Prérequis"]],
    ["Descriptif du cours", ["Descriptif"]],
    ["Compétence(s) visée(s)", ["Compétence(s) visée(s)"]],
    ["Contrôle des connaissances", ["Informations sur l'évaluation"]],
    ["Langue d'enseignement", ["Langue d'enseignement"]],
    ["Bibliographie sélective", ["Bibliographie sélective"]]
]));
guidesFilters.set("default", new Map([
    ["Enseignement", ["Nom EC"] ],
    ["Enseignant.e.s", ["Enseignant.e.s"]],
    ["Volume horaire", ["Volume horaire encadré", "h ",  
						"(", "Heures CM", "h CM / ", "Heures TD", "h TD)"]],
    ["Pré-requis", ["Prérequis"]],
    ["Compétence(s) visée(s)", ["Compétence(s) visée(s)"]],
    ["Descriptif du cours", ["Descriptif"]],
    ["Évaluation", ["Informations sur l'évaluation"]],
    ["Langue d'enseignement", ["Langue d'enseignement"]],
    ["Bibliographie sélective", ["Bibliographie sélective"]]
]));


let fullNames = new Map();
fullNames.set("ANI", "Affaires et négociation internationales");
fullNames.set("TCI", "Techniques du commerce international");
fullNames.set("TCI2", "Techniques du Commerce International");
fullNames.set("RICI", "Relations interculturelles et coopération internationale");
fullNames.set("MPT", "Management de projets touristiques");
fullNames.set("TSM", "Traduction spécialisée multilingue");
fullNames.set("Pro", " - En Contrat pro");
fullNames.set("App", " - En Apprentissage");

//read csv
console.log("Reading csv file "+csvFile);
var csv = fs.readFileSync(csvFile, 'utf8');
Papa.parse(csv, {
	delimiter:",",
	header:true,
	complete:function(results) {
		//console.log(results.data);

		//fill a structure for each year/specialisation
		parseGuides(results.data);

		//save all year/specialisation to separate docs
		saveAllToDocs();
	}
});

function findFicheInSemester(id, sem) {
	for(let f=0; f<sem.length; f++) {
		if(id == sem[f]["UE"]+sem[f]["EC"]+sem[f]["Nom EC"]) {
			return f;
		}
	}
	return -1;
}

//Build a structure of fiches per specialisation/year
function parseGuides(fiches) {
	console.log("Building guides structure");
	//Go through all the fiches
	for(let f of fiches) {
		let guideNames = [];
		
		//First decide in which guide(s) the fiche should go
		if(f["Année"]=="L1" || f["Année"]=="L2") { // L1 L2
			guideNames.push(f["Année"]+" LEA");
		}
		else if(f["Année"]=="L3") { // L3
			let parcours = f["Parcours"].split(",");
			for(let p of parcours) {
				if(p.includes("appliqués aux affaires")) {
					guideNames.push(f["Année"]+" LEA");
				}
				else if(p.includes("commerce international")){
					guideNames.push(f["Année"]+" TCI");
				}
				else {
					guideNames.push(f["Année"]+" MPT");
				}
			}
		}
		else if(f["Parcours"]==fullNames.get("TSM")) { // TSM
			guideNames.push(f["Année"]+" TSM");
		}
		else if(f["Parcours"]=="Master LEA - Tronc commun") {
			guideNames.push(f["Année"]+" LEA TCI");
			guideNames.push(f["Année"]+" LEA RICI");
			guideNames.push(f["Année"]+" LEA MPT");
			guideNames.push(f["Année"]+" LEA ANI");

			//Si on est en UE4 ou UE5 en M1/M2 Pro, ne pas prendre l'UE Tronc Commun LEA 
			if(f["UE"]!=4 && f["UE"]!=5) {  
				guideNames.push(f["Année"]+" LEA TCI Alternance");
				guideNames.push(f["Année"]+" LEA RICI Alternance");
				guideNames.push(f["Année"]+" LEA ANI Alternance");
				guideNames.push(f["Année"]+" LEA MPT Alternance");
			}

			if(f["Nom EC"].includes("Séminaire Transversal")) {
				guideNames.push(f["Année"]+" LEA TCI Alternance");
				guideNames.push(f["Année"]+" LEA RICI Alternance");
				guideNames.push(f["Année"]+" LEA ANI Alternance");
				guideNames.push(f["Année"]+" LEA MPT Alternance");
			}
		}
		else if(f["Parcours"]=="Master LEA - Projet de l'étudiant") {
			guideNames.push(f["Année"]+" LEA TCI");
			guideNames.push(f["Année"]+" LEA RICI");
			guideNames.push(f["Année"]+" LEA MPT");
			guideNames.push(f["Année"]+" LEA ANI");
		}
		else if(f["Parcours"]=="Master Apprentissage - Contrat Pro - Tronc commun") {
			guideNames.push(f["Année"]+" LEA TCI Alternance");
			guideNames.push(f["Année"]+" LEA RICI Alternance");
			guideNames.push(f["Année"]+" LEA ANI Alternance");
			guideNames.push(f["Année"]+" LEA MPT Alternance");
		}
		else if(f["Parcours"]==fullNames.get("ANI")) {
			guideNames.push(f["Année"]+" LEA ANI");
		}
		else if(f["Parcours"]==fullNames.get("ANI")+fullNames.get("Pro")) {
			guideNames.push(f["Année"]+" LEA ANI Alternance");
		}
		else if(f["Parcours"]==fullNames.get("TCI") || 
					f["Parcours"]==fullNames.get("TCI2")) {
			guideNames.push(f["Année"]+" LEA TCI");
		}
		else if(f["Parcours"]==fullNames.get("TCI")+fullNames.get("Pro") ||
				f["Parcours"]==fullNames.get("TCI2")+fullNames.get("Pro")) {
			guideNames.push(f["Année"]+" LEA TCI Alternance");
		}
		else if(f["Parcours"]==fullNames.get("RICI")) {
			guideNames.push(f["Année"]+" LEA RICI");
		}
		else if(f["Parcours"]==fullNames.get("RICI")+fullNames.get("Pro")) {
			guideNames.push(f["Année"]+" LEA RICI Alternance");
		}
		else if(f["Parcours"]==fullNames.get("MPT")) {
			guideNames.push(f["Année"]+" LEA MPT");
		}
		else if(f["Parcours"]==fullNames.get("MPT")+fullNames.get("App")) {
			guideNames.push(f["Année"]+" LEA MPT Alternance");
		}


		//For each guide in which the fiche should be added
		//make modifications
		for(let guideName of guideNames) {
			//if year / specialisation does not already exists
			if(!guides.has(guideName)) {
				guides.set(guideName, new Map());
			//	console.log("Creating guide", guid_str);
			}
			let guid = guides.get(guideName);

			//if semester does not already exists
			if(!guid.has(f["Semestre"])) {
			//	console.log("Creating semester", f["Semestre"], 
			//				"for", guid_str);
				guid.set(f["Semestre"], []);
			}

			//add the fiche to the specialisation/year/semester
			let sem = guid.get(f["Semestre"]);
			let id = f["UE"]+f["EC"]+f["Nom EC"];
			if(guideName.includes("Pro")) { //specific tests for Master Pro
				let existing = findFicheInSemester(id, sem);
				//if the fiche already exists
				if(existing>=0) {
					//if new one is pro, replace old one
					if(!sem[existing]["Parcours"].includes("Pro")) {
						sem[existing] = JSON.parse(JSON.stringify(f));
					}
				}
				else {
					sem.push(JSON.parse(JSON.stringify(f)));
				}
			}
			else { //add in all the other cases
				sem.push(JSON.parse(JSON.stringify(f)));
			}

			//change the EC Name for co-build languages
			/*
			for(let lang of ["portugais", "suédois",
							 "chinois", "néerlandais", "russe"]) {
				if((f["Parcours"]+"").includes(lang)) {
					sem[sem.length-1]["Nom EC"]=lang[0].toUpperCase()
												+lang.substr(1)+" : "
												+sem[sem.length-1]["Nom EC"];
					//sem[sem.length-1]["Nom UE"]+=" - "
					//							 +lang[0].toUpperCase()
					//							 +lang.substr(1);
				}
			}*/
			if((f["Nom UE"]+"").includes("LV2") 
					|| (f["Nom UE"]+"").includes("langue étrangère B")
					|| (f["Nom UE"]+"").includes("socio-culturelle")) {

				if((f["Nom EC"]+"").includes("Anglais")) {
					sem[sem.length-1]["Nom UE"]+=" - Anglais";
				}
				else {
					for(let lang of ["portugais", "suédois", "espagnol",
									 "allemand", "italien", "français",
									 "polonais",
									 "chinois", "néerlandais", "russe"]) {
						if((f["Parcours"]+"").includes(lang)) {
							sem[sem.length-1]["Nom UE"]+=" - "
													 +lang[0].toUpperCase()
													 +lang.substr(1);
						}
					}
				}
			}

			//add enseignants référents to enseignants if not already there
			/*
			if(!(f["Enseignant.e.s"]+"")
					.includes(f["Enseignant.e référent.e"].split(" ")[0])){

				sem[sem.length-1]["Enseignant.e.s"] = 
					f["Enseignant.e référent.e"];

				if(f["Enseignant.e.s"].length>0) {
					sem[sem.length-1]["Enseignant.e.s"]
						+=", "+f["Enseignant.e.s"];
				}
			}*/


			//change UE number depending on parcours for L3
			if(f["Année"]=="L3" 
					&& (guideName=="L3 TCI" || guideName=="L3 MPT")) {
				if(f["UE"]==7 && (f["Nom UE"]+"").includes("Projet")) {
					sem[sem.length-1]["UE"]=5;
				}
				else if(f["UE"]==4 && (f["Nom UE"]+"").includes("anglais")) {
					sem[sem.length-1]["UE"]=3;
				}
				else if(f["UE"]==5 && (f["Nom UE"]+"").includes("LV2")) {
					sem[sem.length-1]["UE"]=4;
				}
				else if(f["UE"]==6 && (f["Nom UE"]+"").includes("LV3")) {
					sem[sem.length-1]["UE"]=4;
				}
			}

			//change UE number in Master RICI
			if(guideName=="M1 LEA RICI" || guideName=="M2 LEA RICI") {
				if(f["Semestre"]=="2") {
					if(f["UE"]=="9" && (f["Nom UE"]+"").includes("Projet")) {
						sem[sem.length-1]["UE"]="8.3";
					}
				}
				else if(f["Semestre"]=="3") {
					if(f["UE"]=="10") {
						sem[sem.length-1]["UE"]="9.2";
					}
				}
			}
		}
	}

	for(let guideName of guides.keys()) {
		let guid = guides.get(guideName);
		let semesters = [];
		for(let semStr of guid.keys()) {
			let sem = guid.get(semStr);
			//sort according to UE/EC
			sem.sort(function(a,b) {
				return (a["UE"]+a["Nom UE"]+a["Nom EC"])
							.localeCompare(b["UE"]+b["Nom UE"]+b["Nom EC"], 
										   'fr', {ignorePunctuation:true, 
										   		  numeric:true});
			});

			if(guideName=="M1 LEA TCI Pro") {
				for(let ue of sem) {
					console.log(ue["UE"]+" "+ue["Nom UE"]+" "
								+ue["Nom EC"]+" "+ue["Parcours"]);
				}
			}
		}
	}
}

//Function to simplify the code below / avoid writing labels twice
function addLine(guide, row, label, suffix) {
	guide.str += "**"+label+"** : "+row[label]+suffix; 
	guide.str += "\n\n";
}


function filterField(field) {
	let fieldText = field;

	//remove (MPT/TCI) if needed
	if(fieldText.endsWith("(MPT/TCI)")) {
		fieldText = fieldText.substr(0, fieldText.length-9);
	}
	
	//remove extra lines of type (<p><br /></p>)
	fieldText = fieldText.replace(/<p><br \/><\/p>/g, "");

	//remove span for lang
	fieldText = fieldText.replace(/<span*>/g, "");
	fieldText = fieldText.replace(/<\/span>/g, "");

	//remove last <br/> if there is one
	if(fieldText.endsWith("<br />")) {
		fieldText = fieldText.substr(0, fieldText.length-6);
	}

	//add a br if the field ends with a list
	if(fieldText.endsWith("/ul>")) {
		fieldText += "&nbsp";
	}

	return fieldText;
}

function writeFilteredFicheToStr(guide, guideName, f) {
	let guidFilt = guidesFilters.get(guideName);
	if(guidFilt==undefined) {
		guidFilt = guidesFilters.get("default");
	}

	const empty = /\s*/;

	guide.str += "<p>\n";
	guide.str += "<i> Université de Lille - UFR LEA - ";
	switch(guideName) {
		case "L1 LEA" : 
		case "L2 LEA" : 
		case "L3 LEA" :  {
			guide.str += f["Année"]+" Langues Étrangères Appliquées aux Affaires";
		}break;
		default : {
			guide.str += guideName;
		}break;
	}
	guide.str += " - Semestre "+f["Semestre"]+"</i>\n";
	guide.str += "</p>\n";
	guide.str += "<p>&nbsp</p>\n";

	guide.str += "<p>\n";
	//guide.str += "<span custom-style='Titre 1 Car1'>S"+f["Semestre"]+" ";
	guide.str += "<span custom-style='Titre 1 Car1'>UE "+f["UE"]+"&nbsp;: </span> ";
	guide.str += "<span custom-style='Titre 1 Car1'>"+f["Nom UE"]+"</span>\n";
	guide.str += "</p>\n";
	guide.str += "<p>&nbsp</p>\n";

	let titlePre = "<p>\n<span custom-style='Subtle Reference'>";
	let titleSuf = "&nbsp;: </span> ";
	let fieldSuf = "</p>\n";

	guide.str += titlePre+ "Enseignement" + titleSuf;
	guide.str += filterField(f["Nom EC"]);
	guide.str += fieldSuf;

    guide.str += titlePre+ "Enseignant.e.s" + titleSuf;
	guide.str += filterField(f["Enseignant.e.s"]);
	guide.str += fieldSuf;

    guide.str += titlePre+ "Volume horaire" + titleSuf;
	guide.str += f["Volume horaire encadré"]+"h ";
	if(f["Heures CM"]=="") {
		guide.str += "(TD)";
	}
	else if(f["Heures TD"]=="") {
		guide.str += "(CM)";
	}
	else {
		guide.str += "("+f["Heures CM"]+"h CM, "+f["Heures TD"]+"h TD)";
	}
	guide.str += fieldSuf;

    guide.str += titlePre+ "Pré-requis" + titleSuf;
	guide.str += filterField(f["Prérequis"])
	guide.str += fieldSuf;

    guide.str += titlePre+ "Compétence(s) visée(s)" + titleSuf;
	guide.str += filterField(f["Compétence(s) visée(s)"]);
	guide.str += fieldSuf;

    guide.str += titlePre+ "Descriptif du cours" + titleSuf;
	guide.str += filterField(f["Descriptif"]);
	guide.str += fieldSuf;

    //guide.str += titlePre+ "Évaluation" + titleSuf;
    guide.str += titlePre+ "Informations supplémentaires sur l'évaluation" + titleSuf;
	guide.str += filterField(f["Informations sur l'évaluation"]);
	guide.str += fieldSuf;

    guide.str += titlePre+ "Langue d'enseignement" + titleSuf;
	guide.str += filterField(f["Langue d'enseignement"]);
	guide.str += fieldSuf;

    guide.str += titlePre+ "Bibliographie sélective" + titleSuf;
	guide.str += filterField(f["Bibliographie sélective"]);
	guide.str += fieldSuf;

	/*
	for(let destField of guidFilt.keys()) {
		guide.str += "<p>\n";
		guide.str += "<span custom-style='Subtle Reference'>"+destField+"&nbsp;: </span> ";
		for(let origField of guidFilt.get(destField)) {
			if(f[origField]!=undefined && !(destField==")) {
				let fieldText = f[origField];


				//remove (MPT/TCI) if needed
				if(fieldText.endsWith("(MPT/TCI)")) {
					fieldText = fieldText.substr(0, fieldText.length-9);
				}
				
				//remove extra lines of type (<p><br /></p>)
				fieldText = fieldText.replace(/<p><br \/><\/p>/g, "");

				//remove span for lang
				fieldText = fieldText.replace(/<span*>/g, "");
				fieldText = fieldText.replace(/<\/span>/g, "");

				//remove last <br/> if there is one
				if(fieldText.endsWith("<br />")) {
					fieldText = fieldText.substr(0, fieldText.length-6);
				}

				//add a br if the field ends with a list
				if(fieldText.endsWith("/ul>")) {
					fieldText += "&nbsp";
				}

				//write the field
				guide.str += fieldText+" "; 
			} 
			else {
				guide.str += origField;
			}
		}
		guide.str += "</p>\n";
	}
	*/

	guide.str +="<div custom-style='Page Break'> \n";
	guide.str += "<br/> </div>\n";
}

function writeMCCTableToStr(guide, guideName, sem) {
	const empty = /\s*/;

	guide.str += "<p>\n";
	guide.str += "<i> Université de Lille - UFR LEA - "+guideName;
	guide.str += " - Semestre "+sem+"</i>\n";
	guide.str += "</p>\n";
	guide.str += "<p>&nbsp</p>\n";

	guide.str += "<p>\n";
	guide.str += "<span custom-style='Titre 1 Car1'>Modalités de contrôle des connaissances</span> ";
	guide.str += "</p>\n";
	guide.str += "<p>&nbsp</p>\n";

	guide.str += "<table>\n";
	guide.str += "<tr>\n";
	guide.str += "<th>UE - EC</th><th>Session initiale</th><th>Rattrapage</th>\n";
	guide.str += "</tr>\n";

	for(let f of guides.get(guideName).get(sem)) {
		guide.str += "<tr>\n";

		guide.str += "<td>\n";
		guide.str += "UE "+f["UE"]+" : ";
		guide.str += f["Nom UE"]+" - "+f["Nom EC"]+"\n";
		guide.str += "</td>\n";

		guide.str += "<td>\n";
		guide.str += f["Evaluation session 1: CC/CT"]+" - ";
		guide.str += f["Evaluation session 1: type d'exercices"]+" - ";
		guide.str += f["Evaluation session 1: écrit/oral"]+" - ";
		guide.str += f["Evaluation session 1: durée de l'épreuve"]+"\n";
		guide.str += "</td>\n";

		guide.str += "<td>\n";
		guide.str += f["Evaluation session 2: CC intégral/CT"]+" - ";
		guide.str += f["Evaluation session 2: type d'exercices"]+" - ";
		guide.str += f["Evaluation session 2: écrit/oral"]+" - ";
		guide.str += f["Evaluation session 2: durée de l'épreuve"]+"\n";
		guide.str += "</td>\n";

		guide.str += "</tr>\n";
	}

	guide.str += "</table>\n";

	guide.str +="<div custom-style='Page Break'> \n";
	guide.str += "<br/> </div>\n";
}

function updateTablesInOdt(input, output) {
	fs.readFile(input, function(err, data) {
		if (err) throw err;
		JSZip.loadAsync(data).then(function (zip) {
			zip.forEach(function (relativePath, zipEntry) {  // 2) print entries
				if(relativePath=="content.xml") {
					zipEntry.async("string")
					.then(function success(content) {

						//modify all tables
						const $ = cheerio.load(content, {xmlMode:true});
						let cells = $("style\\:table-cell-properties").each(
							function(index, element) {
								element.attribs["fo:border"]="solid 0.1pt #000000";
							});

						//write back to file
						zip.file("content.xml", $.xml());

						//reassign the content
						zip
						.generateNodeStream({type:'nodebuffer',streamFiles:true})
						.pipe(fs.createWriteStream(output))
						.on('finish', function () {
							//console.log("Wrote output file");
						});

					}, function error(e) {
						// handle the error
					});

				}
			});
		});
	});
}



function saveToDoc(guideName) {
	let guideFileName = "output/guide-"+String(guideName).trim().replace(/\ /g, "_");
	guideFileName = guideFileName.replace('(', "").replace(')',"").replace("'","_");


	let guide = {};
	guide.str = "";

	//add all the fiches in semester order
	let semesters = [];
	for(let sem of guides.get(guideName).keys()) {
		semesters.push(sem);
	}
	semesters.sort();
	for(let sem of semesters) {
		for(let f of guides.get(guideName).get(sem)) {
			writeFilteredFicheToStr(guide, guideName, f);
		}
		if(!guideName.includes("M1") && !guideName.includes("M2")) {
			writeMCCTableToStr(guide, guideName, sem);
		}
	}

	//Write the html file
	fs.writeFileSync(guideFileName+".html", guide.str, {"encoding":"utf-8"});
	console.log("Done building "+guideFileName+".html");

	//Transform from html to markdown
	let commandHTML = "pandoc ";
	commandHTML+=" -f html "+guideFileName+".html ";
	commandHTML+=" -o "+guideFileName+".md";
	exec(commandHTML, 
		(error, stdout, stderr) => {
			if (error) {
				console.log(`error: ${error.message}`);
				return;
			}

			//Transfrom markdown to doc/odt
			let commandODT = "pandoc ";
			commandODT+=" --lua-filter src/pagebreak.lua ";
			commandODT+=" --lua-filter src/odt-custom-styles.lua ";
			commandODT+=" -f markdown "+guideFileName+".md ";
			commandODT+=" -t odt --reference-doc='input/reference_magali.odt' ";
			commandODT+=" -o "+guideFileName+".odt_tmp ";

			exec(commandODT, 
				(error, stdout, stderr) => {
					if (error) {
						console.log(`error: ${error.message}`);
						return;
					}

					//correct tables
					updateTablesInOdt(guideFileName+".odt_tmp", guideFileName+".odt");
					
					/*
					let commandPDF = "libreoffice --headless --convert-to pdf";
					commandPDF+=guideFileName+".odt --outdir output";

					//Convert odt to pdf using libreoffice
					exec(commandPDF,
							(error, stdout, stderr) => {
								if (error) {
									console.log(`error: ${error.message}`);
									return;
								}
							});*/
				});
	});

}

function saveAllToDocs() {
	console.log("Writing to docs");

	//Loop on all specialisations
	for(let guid of guides.keys()) {
		//console.log(guides.get(guid));
		saveToDoc(guid);
	}
	console.log("Done converting");
}





